package me.powercas_gamer.core.Commands.Teleport;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class TeleportAllCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.teleportall")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
                return true;
            } else {
                for (Player o : Bukkit.getOnlinePlayers()) {
                    if (o.getPlayer() != player) {
                        if (o == null) {
                            sender.sendMessage(style("&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                        } else {
                            sender.sendMessage(style(getConfig().getString("messages.teleporting-targets-to-yourself").replaceAll("%players%", "test")));
                            o.sendMessage(style(getConfig().getString("messages.teleportall.teleporting").replaceAll("%player%", sender.getName())));
                            o.teleport(player);
                        }
                        if (player.hasPermission("core.command.teleportall")) {
                            Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has teleported everyone to " + player.getName() + "&7&o]"));
                        }
                    }
                }
            }
        }
        return true;
    }
}