// Package Name
package me.powercas_gamer.core;

// powercas_gamer's Imports (Commands)
import me.powercas_gamer.core.Commands.Chat.ClearChatCMD;
import me.powercas_gamer.core.Commands.CoreCMD;
import me.powercas_gamer.core.Commands.Gamemode.AdventureCMD;
import me.powercas_gamer.core.Commands.Gamemode.CreativeCMD;
import me.powercas_gamer.core.Commands.Gamemode.GamemodeCMD;
import me.powercas_gamer.core.Commands.Gamemode.SurvivalCMD;
import me.powercas_gamer.core.Commands.Item.GiveCMD;
import me.powercas_gamer.core.Commands.Item.ItemDbCMD;
import me.powercas_gamer.core.Commands.Messages.BroadcastCMD;
import me.powercas_gamer.core.Commands.Messages.IgnoreCMD;
import me.powercas_gamer.core.Commands.Messages.RawBroadcastCMD;
import me.powercas_gamer.core.Commands.Other.ListCMD;
import me.powercas_gamer.core.Commands.Other.WhoisCMD;
import me.powercas_gamer.core.Commands.Player.ClearCMD;
import me.powercas_gamer.core.Commands.Player.FeedCMD;
import me.powercas_gamer.core.Commands.Player.FlyCMD;
import me.powercas_gamer.core.Commands.Player.HealCMD;
import me.powercas_gamer.core.Commands.Teleport.SpawnCMD;
import me.powercas_gamer.core.Commands.Teleport.TeleportAllCMD;
import me.powercas_gamer.core.Commands.Teleport.TeleportCMD;
import me.powercas_gamer.core.Commands.Teleport.TeleportHereCMD;

// powercas_gamer's Imports (Listeners)
import me.powercas_gamer.core.Listeners.ChatFormatListener;
import me.powercas_gamer.core.Listeners.PlayerJoinListener;
import me.powercas_gamer.core.Listeners.PlayerQuitListener;

// Bukkit Imports
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

// Java Imports
import java.util.logging.Level;

public final class Core extends JavaPlugin {
    private static Core instance;
    private static Chat chat = null;
    public static boolean vaultEnabled = true;

    @Override
    public void onEnable() {
        // Instance
        instance = this;
        setupChat();
        // Vault Shit
        if (!setupChat()) {
            getLogger().log(Level.WARNING, "Vault intergration failed. Using defaults");
            vaultEnabled = false;
        }
        // Startup Messages
        System.out.println("[Core] Enabling plugin...");
        // Registering Commands Messages
        System.out.println("[Core] Registering Commands...");
        // Register Commands
        registerCommands();
        //  Successfully registered Commands Messages
        System.out.println("[Core] Successfully registered commands!");
        // Registering Listeners Message
        System.out.println("[Core] Regstering Listeners...");
        // Register Listeners
        registerListeners();
        // Successfully Registered Listeners Message.
        System.out.println("[Core] Successfully registerd listeners!");
        // Registering Config Message
        System.out.println("[Core] Registering Config Files...");
        // Register Config
        registerConfig();
        // Successfully registered config files
        System.out.println("[Core] Successfully registered config files!");
        // Developer
        System.out.println("[Core] This plugin was developed by powercas_gamer");

    }

    @Override
    public void onDisable() {
        // Instance
        instance = null;
        // Disable Messages
        System.out.println("[Core] Disabling plugin...");
        // Save Config
        saveConfig();
    }

    private void registerCommands() {
        getCommand("core").setExecutor(new CoreCMD());
        getCommand("teleport").setExecutor(new TeleportCMD());
        getCommand("teleporthere").setExecutor(new TeleportHereCMD());
        getCommand("teleportall").setExecutor(new TeleportAllCMD());
        getCommand("spawn").setExecutor(new SpawnCMD());
        getCommand("gamemode").setExecutor(new GamemodeCMD());
        getCommand("survival").setExecutor(new SurvivalCMD());
        getCommand("creative").setExecutor(new CreativeCMD());
        getCommand("adventure").setExecutor(new AdventureCMD());
        getCommand("give").setExecutor(new GiveCMD());
        getCommand("fly").setExecutor(new FlyCMD());
        getCommand("broadcast").setExecutor(new BroadcastCMD());
        getCommand("rawbroadcast").setExecutor(new RawBroadcastCMD());
        getCommand("feed").setExecutor(new FeedCMD());
        getCommand("heal").setExecutor(new HealCMD());
        getCommand("clear").setExecutor(new ClearCMD());
        getCommand("clearchat").setExecutor(new ClearChatCMD());
        getCommand("ignore").setExecutor(new IgnoreCMD());
        getCommand("itemdb").setExecutor(new ItemDbCMD());
        getCommand("list").setExecutor(new ListCMD());
        getCommand("whois").setExecutor(new WhoisCMD());
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new ChatFormatListener(), this);
    }

    private void registerConfig() {
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }
        saveDefaultConfig();
        getConfig();
    }

    public static Core getInstance() {
        return instance;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    public static Chat getChat() {
        return chat;
    }
}
