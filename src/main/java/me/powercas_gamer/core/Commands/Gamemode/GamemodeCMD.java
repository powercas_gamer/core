package me.powercas_gamer.core.Commands.Gamemode;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;
import static org.bukkit.GameMode.*;

public class GamemodeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.gamemode")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/gamemode &b<survival|creative|adventure> [player]"));
            } else {
                if (args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s") || args[0].equalsIgnoreCase("0")) {
                    player.sendMessage(style(getConfig().getString("messages.survival.change")));
                    player.setGameMode(SURVIVAL);
                }
                if (args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c") || args[0].equalsIgnoreCase("1")) {
                    player.sendMessage(style(getConfig().getString("messages.creative.change")));
                    player.setGameMode(CREATIVE);
                }
                if (args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("a")) {
                    player.sendMessage(style(getConfig().getString("messages.adventure.change")));
                    player.setGameMode(ADVENTURE);
                } else {
                    if (!player.hasPermission("core.command.survival.other")) {
                        sender.sendMessage(style(getConfig().getString("server.no_permission")));
                    }
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                    } else {
                        if (args[1].equalsIgnoreCase("survival") || args[1].equalsIgnoreCase("s") || args[1].equalsIgnoreCase("0")) {
                            player.sendMessage(style(getConfig().getString("messages.survival.change-other")));
                            target.sendMessage(style(getConfig().getString("messages.survival.change")));
                            target.setGameMode(SURVIVAL);
                        }
                        if (args[1].equalsIgnoreCase("creative") || args[1].equalsIgnoreCase("c") || args[1].equalsIgnoreCase("1")) {
                            player.sendMessage(style(getConfig().getString("messages.creative.change-other")));
                            target.sendMessage(style(getConfig().getString("messages.creative.change")));
                            target.setGameMode(CREATIVE);
                        }
                        if (args[1].equalsIgnoreCase("adventure") || args[1].equalsIgnoreCase("a") || args[1].equalsIgnoreCase("2")) {
                            player.sendMessage(style(getConfig().getString("messages.adventure.change-other")));
                            target.sendMessage(style(getConfig().getString("messages.adventure.change")));
                            target.setGameMode(ADVENTURE);
                        }
                    }
                }
            }
        }
        return true;
    }
}
