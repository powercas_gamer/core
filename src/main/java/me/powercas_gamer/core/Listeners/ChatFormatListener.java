package me.powercas_gamer.core.Listeners;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static me.powercas_gamer.core.Core.getChat;
import static me.powercas_gamer.core.Core.vaultEnabled;
import static me.powercas_gamer.core.Utils.Style.style;

public class ChatFormatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();

        if (!vaultEnabled) {
            String message = e.getMessage();
            String name = p.getDisplayName();
            //String prefix = ;
            if (!p.hasPermission("core.chat.color")) {
                e.setFormat(name + "§7: §f" + message);
            } else {
                if (p.hasPermission("core.chat.color")) {
                    e.setFormat(style(name + "§7: §f" + message));
                }
            }
        } else {
            World world = p.getWorld();
            String group = getChat().getPrimaryGroup(p);
            String prefix = getChat().getGroupPrefix(world, group);
            String suffix = getChat().getGroupSuffix(world, group);
            String message = e.getMessage();
            if (!p.hasPermission("core.chat.color")) {
                e.setFormat(style(prefix + p.getName() + suffix ) + "§7: §f" + message);
            } else {
                if (p.hasPermission("core.chat.color")) {
                    e.setFormat(style(prefix + p.getName() + suffix + "§7: §f" + message));
                    return;
                }
            }
        }
    }
}
