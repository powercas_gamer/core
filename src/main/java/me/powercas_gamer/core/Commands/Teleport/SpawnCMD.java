package me.powercas_gamer.core.Commands.Teleport;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class SpawnCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.spawn")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            player. teleport(Bukkit.getWorld("world").getSpawnLocation());
            player.sendMessage(style(getConfig().getString("messages.spawn.teleporting")));
        }
        if (sender.hasPermission("core.command.spawn")) {
            Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has teleported to spawn&7&o]"));
        } else {
            if (args[0].equalsIgnoreCase("all")) {
                if (!sender.hasPermission("core.command.spawn.all")) {
                    sender.sendMessage(style(getConfig().getString("server.no_permission")));
                }
                for (Player o : Bukkit.getOnlinePlayers()) {
                    o.teleport(Bukkit.getWorld("world").getSpawnLocation());
                    o.sendMessage(style(getConfig().getString("messages.spawn.teleporting-to-spawn-by-player")));
                    sender.sendMessage(style(getConfig().getString("messages.spawn.teleporting-all-to-spawn").replaceAll("%players%", "test")));
                }
                if (sender.hasPermission("core.command.spawn.all")) {
                    Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has teleported everyone to spawn&7&o]"));
                }
            }
        }
        return true;
    }
}