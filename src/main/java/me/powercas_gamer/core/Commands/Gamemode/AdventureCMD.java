package me.powercas_gamer.core.Commands.Gamemode;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class AdventureCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.adventure")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                player.setGameMode(GameMode.ADVENTURE);
                player.sendMessage(style(getConfig().getString("messsages.adventure.gamemode")));
            } else {
                if (!player.hasPermission("core.command.adventure.other")) {
                    sender.sendMessage(style(getConfig().getString("server.no_permission")));
                }
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                } else {
                    target.setGameMode(GameMode.ADVENTURE);
                    target.sendMessage(style(getConfig().getString("messsages.adventure.gamemode")));
                    player.sendMessage(style(getConfig().getString("messages.adventure.gamemode-other").replaceAll("%target%", target.getName())));
                }
            }
        }
        return true;
    }
}