package me.powercas_gamer.core.Commands.Messages;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class RawBroadcastCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.rawbroadcast")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style(getConfig().getString("messages.broadcast.usage")));
            } else {
                //for (String b : getConfig().getString("messages.broadcast.raw")) {
                String b = getConfig().getString("messages.broadcast.raw");
                Bukkit.broadcastMessage(style(b.replaceAll("%message%", StringUtils.join(args, " "))));
                }
            }
        return true;
    }
}
