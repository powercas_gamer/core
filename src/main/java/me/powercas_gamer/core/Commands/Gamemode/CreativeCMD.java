package me.powercas_gamer.core.Commands.Gamemode;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class CreativeCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.creative")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                player.setGameMode(GameMode.CREATIVE);
                player.sendMessage("messages.creative.gamemode");
                //player.sendMessage(style(getConfig().getString("messsages.creative.gamemode")));
            } else {
                if (!player.hasPermission("core.command.creative.other")) {
                    sender.sendMessage(style(getConfig().getString("server.no_permission")));
                }
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                } else {
                    target.setGameMode(GameMode.CREATIVE);
                    target.sendMessage(style(getConfig().getString("messsages.creative.gamemode")));
                    player.sendMessage(style(getConfig().getString("messages.creative.gamemode-other").replaceAll("%target%", target.getName())));
                }
            }
        }
        return true;
    }
}