package me.powercas_gamer.core.Commands.Chat;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class ClearChatCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.clearchat")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/clearchat &b<reason>"));
            } else {
                if (!player.hasPermission("core.command.clearchat.bypass")) {
                    for (int x = 0; x < 200; x++) {
                        Bukkit.broadcastMessage(" ");
                    }
                }
                if (player.hasPermission("core.command.clearchat")) {
                    String cc = StringUtils.join(args, " ");
                    Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has cleared the chat for &e" + cc + "&7&o]"));
                }
            }
        }
        return true;
    }
}
