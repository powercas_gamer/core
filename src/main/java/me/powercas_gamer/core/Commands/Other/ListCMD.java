package me.powercas_gamer.core.Commands.Other;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Style.style;

public class ListCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        Player p = (Player) sender;
        if (args.length < 1) {
            sender.sendMessage(style("&7&m----------------------------------------------------"));
            sender.sendMessage(style("&aThere are currently &2" + Bukkit.getOnlinePlayers() + "&aonline out of &2" + Bukkit.getMaxPlayers() + "&a."));
            sender.sendMessage(style("&7&m----------------------------------------------------"));
        }
        return true;
    }
}
