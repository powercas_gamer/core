package me.powercas_gamer.core.Commands.Player;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class FlyCMD implements CommandExecutor {

    private List<Player> fly = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.fly")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                if (!(fly.contains(player))) {
                    player.sendMessage(style(getConfig().getString("messages.fly.enable")));
                    player.setAllowFlight(true);
                    fly.add(player);
                } else {
                    if (fly.contains(player)) {
                        player.sendMessage(style(getConfig().getString("messages.fly.disable")));
                        player.setAllowFlight(false);
                        fly.remove(player);
                    } else {
                        if (!player.hasPermission("core.command.fly.other")) {
                            sender.sendMessage(style(getConfig().getString("server.no_permission")));
                        }
                        Player target = Bukkit.getPlayerExact(args[0]);
                        if (target == null) {
                            sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                        } else {
                            if (!(fly.contains(target))) {
                                target.sendMessage(style(getConfig().getString("messages.fly.other-enable")));
                                target.setAllowFlight(true);
                                fly.add(target);
                            } else {
                                if (fly.contains(target)) {
                                    target.sendMessage(style(getConfig().getString("messages.fly.other-disable")));
                                    target.setAllowFlight(false);
                                    fly.remove(target);
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
