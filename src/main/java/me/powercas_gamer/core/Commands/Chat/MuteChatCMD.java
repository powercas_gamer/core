package me.powercas_gamer.core.Commands.Chat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class MuteChatCMD implements Listener, CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.clearchat")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            chatEnabled = !chatEnabled;

            sender.sendMessage(style(chatEnabled ? "&aGlobal chat has been unmuted." : "&aGlobal chat has been muted."));
        }
        return true;
    }

    private volatile boolean chatEnabled = true;

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e) {
        if (!chatEnabled) {
            e.setCancelled(false);
        }
    }
}
