package me.powercas_gamer.core.Commands.Player;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class ClearCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.clear")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                player.sendMessage(style(getConfig().getString("messages.clear.own")));
                player.getInventory().clear();
                player.getInventory().setBoots(new ItemStack(Material.AIR));
                player.getInventory().setLeggings(new ItemStack(Material.AIR));
                player.getInventory().setChestplate(new ItemStack(Material.AIR));
                player.getInventory().setHelmet(new ItemStack(Material.AIR));
                if (player.hasPermission("core.clear.other")) {
                    Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has cleared " + player.getName() + "'s inventory&7&o]"));
                } else {
                    if (!player.hasPermission("core.command.clear.other")) {
                        sender.sendMessage(style(getConfig().getString("server.no_permission")));
                    }
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                    } else {
                        if (args.length < 1) {
                            sender.sendMessage(style(getConfig().getString("messages.clear.usage")));
                        } else {
                            target.sendMessage(style(getConfig().getString("messages.clear.own")));
                            sender.sendMessage(style(getConfig().getString("messages.clear.other").replaceAll("%player%", target.getName())));
                            target.getInventory().clear();
                            target.getInventory().setBoots(new ItemStack(Material.AIR));
                            target.getInventory().setLeggings(new ItemStack(Material.AIR));
                            target.getInventory().setChestplate(new ItemStack(Material.AIR));
                            target.getInventory().setHelmet(new ItemStack(Material.AIR));
                        }
                        if (player.hasPermission("core.clear.other")) {
                            Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has cleared " + target.getName() + "'s inventory&7&o]"));
                        }
                    }
                }
            }
        }
        return true;
    }
}