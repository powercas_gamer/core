package me.powercas_gamer.core.Commands.Teleport;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class TeleportCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.teleport")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style(getConfig().getString("messages.teleport.usage")));
            } else {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == player) {
                    sender.sendMessage(style(getConfig().getString("messages.teleport.cant-teleport-to-yourself")));
                } else {
                    if (target == null) {
                        sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                    } else {
                        sender.sendMessage(style(getConfig().getString("messages.teleport.teleporting").replaceAll("%target%", target.getName())));
                        player.teleport(target);
                    }
                    if (player.hasPermission("core.command.teleport")) {
                        Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has teleported to " + target.getName() + "&7&o]"));
                    }
                }
            }
        }
        return true;
    }
}
