package me.powercas_gamer.core.Commands.Other;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Core.getChat;
import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class WhoisCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.spawn")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&cUsage: &7/whois &b<player<"));
            } else {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target == null) {
                    sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                } else {
                    World world = target.getWorld();
                    String group = getChat().getPrimaryGroup(target);
                    String prefix = getChat().getGroupPrefix(world, group);
                    String suffix = getChat().getGroupSuffix(world, group);

                    sender.sendMessage(style("long flat line uwu"));
                    sender.sendMessage(style(" "));
                    sender.sendMessage(style("&6[&e" + target.getDisplayName() + "&6]"));
                    sender.sendMessage(style("&7» &eName&7: &f" + target.getName()));
                    sender.sendMessage(style("&7» &eUUID&7: &f" + target.getUniqueId()));
                    sender.sendMessage(style("&7» &eOperator&7: &f" + target.isOp()));
                    sender.sendMessage(style("&7» &eRank&7: &f" + group));
                    sender.sendMessage(style("&7» &ePrefix&7: &f" + prefix));
                    sender.sendMessage(style("&7» &eSuffix&7: &f" + suffix));
                    sender.sendMessage(style("long flat line uwu"));
                }
            }
        }
        return true;
    }
}