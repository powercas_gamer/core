package me.powercas_gamer.core.Commands.Messages;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class IgnoreCMD implements CommandExecutor {

    private List<Player> ignored = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.ignore")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                sender.sendMessage(style("&7&m--------"));
                sender.sendMessage(style("&e&lIgnore &8- &7Help"));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&e/ignore list &8- &7See whos on your ignore list."));
                sender.sendMessage(style("&e/ignore add &8- &7Add someone to your ignore list."));
                sender.sendMessage(style("&e/ignore remove &8- &7Remove someone from your ignore list."));
                sender.sendMessage(style(" "));
                sender.sendMessage(style("&7&m-------------"));
                //sender.sendMessage(style(getConfig().getString("messages.ignore.help")));
            } else {
                if (alias.equalsIgnoreCase("list")) {
                    sender.sendMessage(style("&7&m-----------"));
                    sender.sendMessage(style("&e&lIgnore &8- &7List"));
                    sender.sendMessage(style(" "));
                    sender.sendMessage(style("&eIgnore Players: &d%players%").replaceAll("%players%", "Not implementd Yet"));
                    sender.sendMessage(style(" "));
                    sender.sendMessage(style("&7&m------"));
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == player) {
                        sender.sendMessage(style(getConfig().getString("messages.ignore.cant-add-yourself")));
                    } else {
                        if (target == null) {
                            sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                        } else {
                            if (target == player) {
                                sender.sendMessage(style(getConfig().getString("messages.ignore.cannot-add-yourself")));
                            } else {
                                if (alias.equalsIgnoreCase("add")) {
                                    if (args.length < 1) {
                                        sender.sendMessage(style(getConfig().getString("messages.ignore.add-usage")));
                                    } else {
                                        if (args.length > 50) {
                                            sender.sendMessage("no");
                                        } else {
                                            if (alias.equalsIgnoreCase("remove")) {
                                                if (args.length < 1) {
                                                    sender.sendMessage(style(getConfig().getString("messages.ignore.remove-usage")));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
}
