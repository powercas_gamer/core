package me.powercas_gamer.core.Commands.Player;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.powercas_gamer.core.Utils.Configuration.getConfig;
import static me.powercas_gamer.core.Utils.Style.style;

public class FeedCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("core.command.feed")) {
                sender.sendMessage(style(getConfig().getString("server.no_permission")));
            }
            if (args.length < 1) {
                player.setSaturation(20);
                player.setFoodLevel(20);
                player.sendMessage(style(getConfig().getString("messages.feed.own")));
                if (player.hasPermission("core.staff")) {
                    Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has feeded " + player.getName() + "&7&o]"));
                } else {
                    if (!player.hasPermission("core.command.feed.other")) {
                        sender.sendMessage(style(getConfig().getString("server.no_permission")));
                    }
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(style(getConfig().getString("error.player-not-found")));
                    } else {
                        target.setSaturation(20);
                        target.setFoodLevel(20);
                        target.sendMessage(style(getConfig().getString("messages.feed.own")));
                        player.sendMessage(style(getConfig().getString("messages.feed.other").replaceAll("%player%", target.getName())));
                    }
                    if (player.hasPermission("core.feed.other")) {
                        Bukkit.broadcastMessage(style("&7&o[" + sender.getName() + "&7: has feeded " + target.getName() + "&7&o]"));
                    }
                }
            }
        }
        return true;
    }
}